import { Component } from '@angular/core';

@Component({
	selector: 'first-app', 
	templateUrl: './app/app.component.html',
	styleUrls: ['./app/app.component.css']
})

export class AppComponent {

	constructor(){
		this.items = [{
			text: 'asd',
			isCompleted: false
		},{
			text: 'asdasd',
			isCompleted: false
		}];

		this.newItemText = '';
		window.items = this.items;
	}

	addItem(text){
		this.items.push({
			text: text,
			isCompleted: false
		});
	}

	removeItem(index){
		this.items.splice(index, 1);
	}

}